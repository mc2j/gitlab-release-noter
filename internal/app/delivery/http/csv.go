package http

import (
	"fmt"
	"net/http"
	"strings"
)

func (h *ApiHandler) CsvHandler(w http.ResponseWriter, r *http.Request) {
	onlyNotes := r.URL.Query().Get("onlyNotes") != ""
	projects, rows, err := h.getProjectsAndMrs(r, onlyNotes)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	s := ""
	for _, row := range rows {
		s += fmt.Sprintf(
			"%s;%s;%s;%s;%s;%s\r\n",
			strings.ReplaceAll(strings.ReplaceAll(row.Title, "\n", " "), ";", " "),
			strings.ReplaceAll(strings.ReplaceAll(row.Description, "\n", " "), ";", " "),
			projects[row.ProjectID],
			row.Author,
			row.WebUrl,
			row.JiraUrl,
		)
	}

	w.Header().Set("Content-Disposition", `attachment; filename="changelog.csv"`)
	_, _ = w.Write([]byte(s))
}
