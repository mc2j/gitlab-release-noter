package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/spf13/viper"

	handler "gitlab-release-noter/internal/app/delivery/http"
	"gitlab-release-noter/internal/app/types"
)

func main() {
	configPath := os.Getenv("CONFIG_PATH")
	if configPath == "" {
		configPath = "./config/local.yml"
	}
	viper.SetConfigFile(configPath)

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalln(err)
	}

	var conf types.Config
	if err := viper.Unmarshal(&conf); err != nil {
		log.Fatalln(err)
	}

	h := handler.New(conf)
	r := mux.NewRouter()
	r.HandleFunc("/", h.IndexHandler).Methods(http.MethodGet)
	r.HandleFunc("/report", h.ReportHandler).Methods(http.MethodGet)
	r.HandleFunc("/csv", h.CsvHandler).Methods(http.MethodGet)

	log.Printf("service started on :%d \r\n", conf.Port)

	if err := http.ListenAndServe(fmt.Sprintf(":%d", conf.Port), r); err != nil {
		log.Fatalln(err)
	}
}
