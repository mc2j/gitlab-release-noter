package http

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gitlab-release-noter/internal/app/types"
	"gitlab-release-noter/internal/static"
)

const layout = "02.01.2006 15:04:05"
const gitlabLayout = "2006-01-02T15:04:05"

var jiraTaskRegex = regexp.MustCompile("([A-Za-z]*-[0-9]*)")

func (h *ApiHandler) ReportHandler(w http.ResponseWriter, r *http.Request) {
	onlyNotes := r.URL.Query().Get("onlyNotes") != ""

	projects, rows, err := h.getProjectsAndMrs(r, onlyNotes)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	s := `<!doctype html>
		<html lang="en">
		  <head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<title>Gitlab Release Noter</title>
			<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
			<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.0/bootstrap-table.min.css">

			<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
			<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.0/bootstrap-table.js"></script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.9.1/extensions/editable/bootstrap-table-editable.js"></script>
			<script>` + static.TableFilterJs + `</script>
			<script>
				function urlFormatter(value, row) {
					if (value === '') return '';
					return '<a href = "'+value+'" target="_blank">Ссылка</a>';
				}
			</script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">
			<style>
				.multiselect-container input[type=checkbox] {
					position: relative;
				}
			</style>
			<script>
				$(document).ready(function() {
					$('.multiple-checkboxes').multiselect({
						includeSelectAllOption: true,
					});
				});
			</script>
		  </head>
		  <body>
			<div class = "container">
				<h2>Release-notes</h2>
				<table
					id="table"
					data-toggle="table"
					data-filter-control="true"
					data-show-export="true"
					data-click-to-select="true"
					data-toolbar="#toolbar"
					class = "table table-bordered table-responsive">
				<thead>
				<tr>
					<th data-field="title" data-filter-control="input">Заголовок</th>
					<th data-field="description" data-filter-control="input">Описание</th>
					<th data-field="project" data-filter-control="multiselect" data-sortable="true">Проект</th>
					<th data-field="author" data-filter-control="multiselect" data-sortable="true">Автор</th>
					<th data-field="mr" data-formatter="urlFormatter">MR</th>
					<th data-field="task" data-formatter="urlFormatter">Jira</th>
				</tr>
				</thead>`

	for _, row := range rows {
		s += `<tr>
			<td>` + row.Title + `</td>
			<td>` + row.Description + `</td>
			<td>` + projects[row.ProjectID] + `</td>
			<td>` + row.Author + `</td>
			<td>` + row.WebUrl + `</td>
			<td>` + row.JiraUrl + `</td>
		</tr>`
	}

	s += "</table></div></body></html>"

	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte(s))
}

func (h *ApiHandler) getProjectsAndMrs(r *http.Request, onlyNotes bool) (map[int64]string, []types.MergeRequest, error) {
	tasks := make(map[string]struct{})
	if onlyNotes {
		var err error
		tasks, err = h.getTasks()
		if err != nil {
			return nil, nil, err
		}
	}

	rows, err := h.getMrs(r, tasks, onlyNotes)
	if err != nil {
		return nil, nil, err
	}
	projects, err := h.getProjects()
	if err != nil {
		return nil, nil, err
	}

	return projects, rows, nil
}

func (h *ApiHandler) getProjects() (map[int64]string, error) {
	if h.Cache.Projects != nil {
		log.Println("проекты взяты из кеша")
		return h.Cache.Projects, nil
	}

	rows := make(map[int64]string)
	for i := 1; ; i++ {
		uri, err := url.Parse(fmt.Sprintf("%s/api/v4/projects?per_page=100&page=%d", h.Conf.Gitlab.Host, i))
		if err != nil {
			return nil, err
		}
		req := &http.Request{
			Method: http.MethodGet,
			URL:    uri,
			Header: h.AccessHeader,
		}
		resp, err := h.Client.Do(req)
		if err != nil {
			return nil, err
		}

		buf := new(bytes.Buffer)
		_, err = buf.ReadFrom(resp.Body)
		if err != nil {
			return nil, err
		}

		prs := make([]types.ApiProject, 0)
		err = json.Unmarshal(buf.Bytes(), &prs)
		if err != nil {
			return nil, err
		}

		if len(prs) == 0 {
			break
		}

		for _, pr := range prs {
			rows[pr.ID] = pr.Name
		}

		log.Println("страница проектов № " + strconv.Itoa(i) + " обработана")
	}

	h.Cache.Projects = rows

	return rows, nil
}

func (h *ApiHandler) getMrs(r *http.Request, tasks map[string]struct{}, onlyNotes bool) ([]types.MergeRequest, error) {
	branch := r.URL.Query().Get("branch")
	if branch == "" {
		return nil, errors.New("укажите ветку")
	}
	for _, bb := range h.Conf.Gitlab.BanBranches {
		if strings.TrimSpace(bb) == strings.TrimSpace(branch) {
			return nil, fmt.Errorf("указана запрещенная ветка %s", branch)
		}
	}

	dateStart := r.URL.Query().Get("dateStart")
	dateEnd := r.URL.Query().Get("dateEnd")
	tagStart := r.URL.Query().Get("tagStart")
	tagEnd := r.URL.Query().Get("tagEnd")

	if dateStart != "" {
		dateStart += " 00:00:00"
	}
	if dateEnd != "" {
		dateEnd += " 23:59:59"
	}

	uriSuffix := ""
	if dateStart == "" && tagStart == "" {
		return nil, errors.New("укажите дату начала или тег")
	}

	if tagStart != "" {
		var err error
		dateStart, err = h.getTagDate(tagStart)
		if err != nil {
			return nil, err
		}
	}

	t1, err := time.Parse(layout, dateStart)
	if err != nil {
		return nil, err
	}

	if t1.Before(time.Now().AddDate(0, -3, -1)) {
		return nil, errors.New("дата не должна быть раньше, чем 3 месяца с текущего момента")
	}

	uriSuffix += fmt.Sprintf("&created_after=%s", t1.Format(gitlabLayout))

	if tagEnd != "" {
		var err error
		dateEnd, err = h.getTagDate(tagEnd)
		if err != nil {
			return nil, err
		}
	}

	if dateEnd != "" {
		t2, err := time.Parse(layout, dateEnd)
		if err != nil {
			return nil, err
		}

		uriSuffix += fmt.Sprintf("&created_before=%s", t2.Format(gitlabLayout))
	}

	rows := make([]types.MergeRequest, 0)
	for i := 1; ; i++ {
		mrUrl := fmt.Sprintf("%s/api/v4/merge_requests?state=merged&scope=all&per_page=100&target_branch=%s&page=%d%s", h.Conf.Gitlab.Host, branch, i, uriSuffix)
		uri, err := url.Parse(mrUrl)
		if err != nil {
			return nil, err
		}
		req := &http.Request{
			Method: http.MethodGet,
			URL:    uri,
			Header: h.AccessHeader,
		}
		resp, err := h.Client.Do(req)
		if err != nil {
			return nil, err
		}

		buf := new(bytes.Buffer)
		_, err = buf.ReadFrom(resp.Body)
		if err != nil {
			return nil, err
		}

		mrs := make([]types.ApiMergeRequest, 0)

		err = json.Unmarshal(buf.Bytes(), &mrs)
		if err != nil {
			return nil, err
		}

		if len(mrs) == 0 {
			break
		}

		for _, mr := range mrs {
			jiraTask := jiraTaskRegex.FindString(mr.Title)

			_, ok := tasks[jiraTask]
			if onlyNotes && !ok {
				continue
			}

			rows = append(rows, types.MergeRequest{
				Title:       mr.Title,
				Description: mr.Description,
				ProjectID:   mr.ProjectID,
				Author:      mr.Author.Name,
				WebUrl:      mr.WebUrl,
				JiraUrl:     fmt.Sprintf("%s/browse/%s", h.Conf.Jira.Host, jiraTask),
			})
		}

		log.Println("страница мров № " + strconv.Itoa(i) + " обработана")
	}

	return rows, nil
}

func (h *ApiHandler) getTasks() (map[string]struct{}, error) {
	jiraUrl := h.Conf.Jira.Host + "/rest/api/2/search?maxResults=1000&fields=key&jql=" + url.PathEscape(`"Документация и релиз ноты" is not null`)
	u, err := url.Parse(jiraUrl)
	if err != nil {
		return nil, err
	}

	req := &http.Request{
		Method: http.MethodGet,
		URL:    u,
		Header: h.JiraAccessHeader,
	}
	resp, err := h.Client.Do(req)
	if err != nil {
		return nil, err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return nil, err
	}

	tasks := types.ApiJiraTasks{}
	err = json.Unmarshal(buf.Bytes(), &tasks)
	if err != nil {
		return nil, err
	}

	log.Println("задачи jira получены")

	tasksMap := make(map[string]struct{})
	for _, t := range tasks.Issues {
		tasksMap[t.Key] = struct{}{}
	}

	return tasksMap, nil
}

func (h *ApiHandler) getTagDate(tag string) (string, error) {
	tagInfoUrl := fmt.Sprintf("%s/api/v4/projects/%d/repository/tags/%s", h.Conf.Gitlab.Host, h.Conf.Gitlab.MasterProject, tag)

	uri, err := url.Parse(tagInfoUrl)
	if err != nil {
		return "", err
	}
	req := &http.Request{
		Method: http.MethodGet,
		URL:    uri,
		Header: h.AccessHeader,
	}
	resp, err := h.Client.Do(req)
	if err != nil {
		return "", err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return "", err
	}

	apiTag := types.ApiTag{}

	err = json.Unmarshal(buf.Bytes(), &apiTag)
	if err != nil {
		return "", err
	}

	d := apiTag.Commit.CreatedAt.Format(layout)
	log.Printf("информация о теге %s получена, дата - %s\r\n", tag, d)

	return d, nil
}
