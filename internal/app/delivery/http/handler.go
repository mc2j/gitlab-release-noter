package http

import (
	"crypto/tls"
	"net/http"

	"gitlab-release-noter/internal/app/types"
)

type Cache struct {
	Projects map[int64]string
}

type ApiHandler struct {
	Conf             types.Config
	AccessHeader     http.Header
	JiraAccessHeader http.Header
	Client           *http.Client
	JiraClient       *http.Client
	Cache            Cache
}

func New(conf types.Config) *ApiHandler {
	cli := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}

	header := http.Header{}
	header.Set("Authorization", "Bearer "+conf.Gitlab.Token)

	jiraHeader := http.Header{}
	jiraHeader.Set("Authorization", "Bearer "+conf.Jira.Token)

	return &ApiHandler{
		Conf:             conf,
		AccessHeader:     header,
		JiraAccessHeader: jiraHeader,
		Client:           cli,
		JiraClient:       cli,
		Cache: Cache{
			Projects: nil,
		},
	}
}
