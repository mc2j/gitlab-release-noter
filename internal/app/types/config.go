package types

type Gitlab struct {
	Host          string
	Token         string
	MasterProject int64
	BanBranches   []string
}

type Jira struct {
	Host  string
	Token string
}

type Config struct {
	Port   int64
	Gitlab Gitlab
	Jira   Jira
}
