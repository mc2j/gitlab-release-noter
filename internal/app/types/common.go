package types

import "time"

type ApiMergeRequestAuthor struct {
	Name string `json:"name"`
}

type ApiMergeRequest struct {
	Title       string                `json:"title"`
	Description string                `json:"description"`
	ProjectID   int64                 `json:"project_id"`
	Author      ApiMergeRequestAuthor `json:"author"`
	WebUrl      string                `json:"web_url"`
}

type MergeRequest struct {
	Title       string
	Description string
	ProjectID   int64
	Author      string
	WebUrl      string
	JiraUrl     string
}

type ApiProject struct {
	ID   int64  `json:"id"`
	Name string `json:"path_with_namespace"`
}

type ApiJiraTask struct {
	Key string `json:"key"`
}

type ApiJiraTasks struct {
	Issues []ApiJiraTask `json:"issues"`
}

type ApiTagCommit struct {
	CreatedAt time.Time `json:"created_at"`
}

type ApiTag struct {
	Commit ApiTagCommit `json:"commit"`
}
