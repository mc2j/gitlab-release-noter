package http

import (
	"net/http"
)

func (h *ApiHandler) IndexHandler(w http.ResponseWriter, _ *http.Request) {
	html := `<!doctype html>
		<html lang="en">
		  <head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<title>Gitlab Release Noter</title>
			<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
			<link rel="stylesheet" href= "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
		  </head>
		  <body>
			<div class = "container">
				<h2>Release-noter из МРов</h2>
				<form method = "get">
				  <div class="mb-3">
					<label for="branch" class="form-label">Ветка</label>
					<input type="text" class="form-control" id="branch" name = "branch">
				  </div>
				  <div class="mb-3 row">
					<div class = "col-6">
						<label for="dateStart" class="form-label">Дата от</label>
						<input type="text" class="form-control" id="dateStart" name = "dateStart" aria-describedby="dateStartHelp">
						<div id="dateStartHelp" class="form-text">Обязательно формата 11.01.2023</div>
					</div>
					<div class = "col-6">
						<label for="tagStart" class="form-label">Или тег от</label>
						<input type="text" class="form-control" id="tagStart" name = "tagStart" aria-describedby="tagStartHelp">
					</div>
				  </div>
				  <div class="mb-3 row">
					<div class = "col-6">
						<label for="dateEnd" class="form-label">Дата до</label>
						<input type="text" class="form-control" id="dateEnd" name = "dateEnd" aria-describedby="dateEndHelp">
						<div id="dateEndHelp" class="form-text">Обязательно формата 11.01.2023</div>
					</div>
					<div class = "col-6">
						<label for="tagEnd" class="form-label">Или тег до</label>
						<input type="text" class="form-control" id="tagEnd" name = "tagEnd" aria-describedby="tagEndHelp">
					</div>
				  </div>
				  <div class="mb-3">
					<div class="form-check">
					  <input class="form-check-input" type="checkbox" value="1" id="onlyNotes" name="onlyNotes">
					  <label class="form-check-label" for="onlyNotes">
						Только с требованием к техническим релиз-нотам
					  </label>
					</div>
				  </div>
				  <button type="submit" class="btn btn-primary" formaction = "/report">Показать таблицу</button>
				  <button type="submit" class="btn btn-primary" formaction = "/csv">Скачать csv</button>
				</form>
			  </div>
			</body>
			<script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity= "sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
			<script>
				$(function () {
					let bd = new Date();
					bd.setMonth(bd.getMonth()-3);
					$("#dateStart").datepicker({ 
						autoclose: true, 
						clearBtn: true,
						format: "dd.mm.yyyy",
						language: "ru",
						startDate: bd,
						todayHighlight: true,
					});

					const day = bd.getDate().toString().padStart(2, '0')
					const month = (bd.getMonth() + 1).toString().padStart(2, '0')

					$('#dateStart').datepicker('update', day+'.'+month+'.'+bd.getFullYear());
					$("#dateEnd").datepicker({ 
						autoclose: true, 
						clearBtn: true,
						format: "dd.mm.yyyy",
						language: "ru",
						todayHighlight: true,
					});
				});
			</script>
		    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    		<script src= "https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity= "sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    		<script src= "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
		</html>`

	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte(html))
	return
}
